package al.hexad.assignment.ui.base.view

import al.hexad.assignment.utils.AppConstants
import al.hexad.assignment.utils.CommonUtils
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.View
import com.kaopiz.kprogresshud.KProgressHUD
import dagger.android.support.AndroidSupportInjection

/**
 * Created by Arbër Thaçi on 19-01-05.
 * Email: arberlthaci@gmail.com
 */

abstract class BaseFragment : Fragment(), MVPView {

    private var parentActivity: BaseActivity? = null
    private var loadingIndicator: KProgressHUD? = null

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is BaseActivity) {
            val activity = context as BaseActivity?
            this.parentActivity = activity
            activity?.onFragmentAttached()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        performDependencyInjection()
        setHasOptionsMenu(false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUp()
    }

    override fun showLoadingIndicator() {
        dismissLoadingIndicator()
        loadingIndicator = CommonUtils.showLoadingIndicator(this.context)
    }

    override fun dismissLoadingIndicator() {
        loadingIndicator?.let { if (it.isShowing) it.dismiss() }
    }

    override fun showAlerter(alerterMode: AppConstants.AlerterMode, message: String) {
        parentActivity?.showAlerter(alerterMode, message)
    }

    fun getBaseActivity() = parentActivity

    private fun performDependencyInjection() = AndroidSupportInjection.inject(this)

    abstract fun setUp()

    interface CallBack {
        fun onFragmentAttached()
        fun onFragmentDetached(tag: String, fragmentAnimationMode: AppConstants.FragmentAnimationMode = AppConstants.FragmentAnimationMode.ANIMATION_NONE)
        fun openNewFragment(fragment: Fragment, tag: String, title: String? = null, fragmentAnimationMode: AppConstants.FragmentAnimationMode = AppConstants.FragmentAnimationMode.ANIMATION_NONE)
    }
}