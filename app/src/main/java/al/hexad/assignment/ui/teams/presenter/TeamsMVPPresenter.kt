package al.hexad.assignment.ui.teams.presenter

import al.hexad.assignment.ui.base.presenter.MVPPresenter
import al.hexad.assignment.ui.teams.interactor.TeamsMVPInteractor
import al.hexad.assignment.ui.teams.view.TeamsMVPView

/**
 * Created by Arbër Thaçi on 19-01-05.
 * Email: arberlthaci@gmail.com
 */

interface TeamsMVPPresenter <V : TeamsMVPView, I : TeamsMVPInteractor> : MVPPresenter<V, I> {

    fun onViewPrepared()

    fun onRateTeamClicked(position: Int)
    fun onRateTeamApplied(numberOfStars: Int)

    fun onRandomRateClicked()
}