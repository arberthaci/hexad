package al.hexad.assignment.ui.home

import al.hexad.assignment.ui.home.interactor.HomeInteractor
import al.hexad.assignment.ui.home.interactor.HomeMVPInteractor
import al.hexad.assignment.ui.home.presenter.HomeMVPPresenter
import al.hexad.assignment.ui.home.presenter.HomePresenter
import al.hexad.assignment.ui.home.view.HomeMVPView
import dagger.Module
import dagger.Provides

/**
 * Created by Arbër Thaçi on 19-01-05.
 * Email: arberlthaci@gmail.com
 */

@Module
class HomeActivityModule {

    @Provides
    internal fun provideHomeInteractor(homeInteractor: HomeInteractor): HomeMVPInteractor = homeInteractor

    @Provides
    internal fun provideHomePresenter(homePresenter: HomePresenter<HomeMVPView, HomeMVPInteractor>)
            : HomeMVPPresenter<HomeMVPView, HomeMVPInteractor> = homePresenter
}