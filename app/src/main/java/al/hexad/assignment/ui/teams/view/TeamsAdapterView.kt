package al.hexad.assignment.ui.teams.view

/**
 * Created by Arbër Thaçi on 19-01-05.
 * Email: arberlthaci@gmail.com
 */

interface TeamsAdapterView {

    fun adapterNotifyDataSetChanged()
    fun adapterItemClicked(position: Int)
}