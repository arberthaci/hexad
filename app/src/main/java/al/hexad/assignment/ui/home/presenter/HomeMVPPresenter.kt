package al.hexad.assignment.ui.home.presenter

import al.hexad.assignment.ui.base.presenter.MVPPresenter
import al.hexad.assignment.ui.home.interactor.HomeMVPInteractor
import al.hexad.assignment.ui.home.view.HomeMVPView

/**
 * Created by Arbër Thaçi on 19-01-05.
 * Email: arberlthaci@gmail.com
 */

interface HomeMVPPresenter<V : HomeMVPView, I : HomeMVPInteractor> : MVPPresenter<V, I>