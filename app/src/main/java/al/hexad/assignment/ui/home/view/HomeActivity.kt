package al.hexad.assignment.ui.home.view

import al.hexad.assignment.R
import al.hexad.assignment.ui.base.view.BaseActivity
import al.hexad.assignment.ui.home.interactor.HomeMVPInteractor
import al.hexad.assignment.ui.home.presenter.HomeMVPPresenter
import al.hexad.assignment.ui.teams.view.TeamsFragment
import al.hexad.assignment.utils.AppConstants
import al.hexad.assignment.utils.extensions.addFragment
import al.hexad.assignment.utils.extensions.removeFragment
import android.os.Bundle
import android.support.v4.app.Fragment
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject

/**
 * Created by Arbër Thaçi on 19-01-05.
 * Email: arberlthaci@gmail.com
 */

class HomeActivity : BaseActivity(), HomeMVPView, HasSupportFragmentInjector {

    @Inject
    internal lateinit var presenter: HomeMVPPresenter<HomeMVPView, HomeMVPInteractor>
    @Inject
    internal lateinit var fragmentDispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        presenter.onAttach(this)
    }

    override fun onDestroy() {
        presenter.onDetach()
        super.onDestroy()
    }

    override fun supportFragmentInjector() = fragmentDispatchingAndroidInjector

    /**
     **********************************
     * HomeMVPView implementation
     * Start
     **********************************
     **/

    override fun setToolbarTitle(title: String) {
        supportActionBar?.title = title
    }

    override fun openTeamsFragment() = openNewFragment(fragment = TeamsFragment.newInstance(),
            tag = TeamsFragment.TAG,
            title = TeamsFragment.TITLE,
            fragmentAnimationMode = AppConstants.FragmentAnimationMode.ANIMATION_FADE)

    /**
     **********************************
     * HomeMVPView implementation
     * End
     **********************************
     **/

    /**
     **********************************
     * BaseFragment.CallBack implementation
     * Start
     **********************************
     **/

    override fun onFragmentAttached() {}

    override fun onFragmentDetached(tag: String, fragmentAnimationMode: AppConstants.FragmentAnimationMode) {
        supportFragmentManager?.removeFragment(tag = tag, fragmentAnimationMode = fragmentAnimationMode)
    }

    override fun openNewFragment(fragment: Fragment, tag: String, title: String?, fragmentAnimationMode: AppConstants.FragmentAnimationMode) {
        supportFragmentManager.addFragment(containerViewId = R.id.home_fragment_container,
                fragment = fragment,
                tag = tag,
                fragmentAnimationMode = fragmentAnimationMode)
        title?.let { setToolbarTitle(it) }
    }

    /**
     **********************************
     * BaseFragment.CallBack implementation
     * End
     **********************************
     **/
}
