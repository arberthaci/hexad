package al.hexad.assignment.ui.teams

import al.hexad.assignment.ui.teams.interactor.TeamsInteractor
import al.hexad.assignment.ui.teams.interactor.TeamsMVPInteractor
import al.hexad.assignment.ui.teams.presenter.TeamsAdapterPresenter
import al.hexad.assignment.ui.teams.presenter.TeamsMVPPresenter
import al.hexad.assignment.ui.teams.presenter.TeamsPresenter
import al.hexad.assignment.ui.teams.view.TeamsAdapter
import al.hexad.assignment.ui.teams.view.TeamsFragment
import al.hexad.assignment.ui.teams.view.TeamsMVPView
import android.support.v7.widget.LinearLayoutManager
import dagger.Module
import dagger.Provides

/**
 * Created by Arbër Thaçi on 19-01-05.
 * Email: arberlthaci@gmail.com
 */

@Module
class TeamsFragmentModule {

    @Provides
    internal fun provideTeamsInteractor(interactor: TeamsInteractor): TeamsMVPInteractor = interactor

    @Provides
    internal fun provideTeamsPresenter(presenter: TeamsPresenter<TeamsMVPView, TeamsMVPInteractor>)
            : TeamsMVPPresenter<TeamsMVPView, TeamsMVPInteractor> = presenter

    @Provides
    internal fun provideTeamsAdapterPresenter(interactor: TeamsInteractor): TeamsAdapterPresenter = TeamsAdapterPresenter(arrayListOf())

    @Provides
    internal fun provideTeamsAdapter(presenter: TeamsAdapterPresenter): TeamsAdapter = TeamsAdapter(presenter)

    @Provides
    internal fun provideLinearLayoutManager(fragment: TeamsFragment): LinearLayoutManager = LinearLayoutManager(fragment.activity)
}