package al.hexad.assignment.ui.teams.interactor

import al.hexad.assignment.ui.base.interactor.BaseInteractor
import javax.inject.Inject

/**
 * Created by Arbër Thaçi on 19-01-05.
 * Email: arberlthaci@gmail.com
 */

class TeamsInteractor @Inject internal constructor() : BaseInteractor(), TeamsMVPInteractor