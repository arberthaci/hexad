package al.hexad.assignment.ui.teams.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import al.hexad.assignment.R
import al.hexad.assignment.data.models.Team
import al.hexad.assignment.ui.base.view.BaseFragment
import al.hexad.assignment.ui.teams.interactor.TeamsMVPInteractor
import al.hexad.assignment.ui.teams.presenter.TeamsMVPPresenter
import al.hexad.assignment.utils.AppConstants
import al.hexad.assignment.utils.extensions.hide
import al.hexad.assignment.utils.extensions.show
import al.hexad.assignment.utils.rvadapterlisteners.OnTeamActionListener
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.list.listItemsSingleChoice
import kotlinx.android.synthetic.main.fragment_teams.*
import javax.inject.Inject

/**
 * Created by Arbër Thaçi on 19-01-05.
 * Email: arberlthaci@gmail.com
 */

class TeamsFragment : BaseFragment(), TeamsMVPView, OnTeamActionListener {

    @Inject
    internal lateinit var teamsAdapter: TeamsAdapter
    @Inject
    internal lateinit var layoutManager: LinearLayoutManager
    @Inject
    internal lateinit var presenter: TeamsMVPPresenter<TeamsMVPView, TeamsMVPInteractor>

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_teams, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        presenter.onAttach(this)
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onDestroyView() {
        presenter.onDetach()
        super.onDestroyView()
    }

    /**
     **********************************
     * BaseFragment implementation
     * Start
     **********************************
     **/

    override fun setUp() {
        layoutManager.orientation = LinearLayoutManager.VERTICAL
        rv_teams.layoutManager = layoutManager
        rv_teams.itemAnimator = DefaultItemAnimator()
        rv_teams.adapter = teamsAdapter
        teamsAdapter.onTeamActionListener = this

        fab_random_rate?.setOnClickListener { presenter.onRandomRateClicked() }

        presenter.onViewPrepared()
    }

    /**
     **********************************
     * BaseFragment implementation
     * End
     **********************************
     **/

    /**
     **********************************
     * OnTeamActionListener implementation
     * Start
     **********************************
     **/

    override fun onItemSelected(position: Int) =
            presenter.onRateTeamClicked(position)

    /**
     **********************************
     * OnTeamActionListener implementation
     * End
     **********************************
     **/

    /**
     **********************************
     * TeamsMVPView implementation
     * Start
     **********************************
     **/

    override fun prepareLoading() {
        showLoadingIndicator()
        fab_random_rate?.hide()
        rv_teams?.hide()
    }

    override fun displayTeamList(teams: List<Team>?): Unit? = teams?.let {
        teamsAdapter.presenter.insertTeamsToList(it)

        rv_teams?.show()
        fab_random_rate?.show()
        dismissLoadingIndicator()
    }

    override fun openRateTeamDialog() {
        MaterialDialog(context!!)
                .title(R.string.dialog_rate_title)
                .listItemsSingleChoice(R.array.dialog_items) { dialog, index, _ ->
                    dialog.dismiss()
                    presenter.onRateTeamApplied(numberOfStars = index + 1)
                }
                .positiveButton(R.string.dialog_rate_label)
                .show()
    }

    override fun displayStartIconInFab() {
        fab_random_rate?.setImageResource(R.drawable.ic_play_arrow_white_24dp)
    }

    override fun displayStopIconInFab() {
        fab_random_rate?.setImageResource(R.drawable.ic_stop_white_24dp)
    }

    override fun displayStartRandomRatingAlert() {
        showAlerter(AppConstants.AlerterMode.ALERT_SUCCESS, getString(R.string.global_random_rating_started))
    }

    override fun displayStopRandomRatingAlert() {
        showAlerter(AppConstants.AlerterMode.ALERT_WARNING, getString(R.string.global_random_rating_stopped))
    }

    override fun displayProblemRandomRatingAlert() {
        showAlerter(AppConstants.AlerterMode.ALERT_FAILED, getString(R.string.global_there_was_an_error_with_random_rating))
    }

    /**
     **********************************
     * TeamsMVPView implementation
     * End
     **********************************
     **/

    companion object {
        internal val TAG = "teams_ft"
        internal val TITLE = "English Teams"

        fun newInstance(): TeamsFragment {
            return TeamsFragment()
        }
    }
}
