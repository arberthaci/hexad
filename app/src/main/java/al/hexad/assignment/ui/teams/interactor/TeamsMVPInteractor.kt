package al.hexad.assignment.ui.teams.interactor

import al.hexad.assignment.ui.base.interactor.MVPInteractor

/**
 * Created by Arbër Thaçi on 19-01-05.
 * Email: arberlthaci@gmail.com
 */

interface TeamsMVPInteractor : MVPInteractor