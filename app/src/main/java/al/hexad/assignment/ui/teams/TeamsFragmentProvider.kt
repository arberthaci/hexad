package al.hexad.assignment.ui.teams

import al.hexad.assignment.ui.teams.view.TeamsFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by Arbër Thaçi on 19-01-05.
 * Email: arberlthaci@gmail.com
 */

@Module
internal abstract class TeamsFragmentProvider {

    @ContributesAndroidInjector(modules = [TeamsFragmentModule::class])
    internal abstract fun provideTeamsFragmentFactory(): TeamsFragment
}