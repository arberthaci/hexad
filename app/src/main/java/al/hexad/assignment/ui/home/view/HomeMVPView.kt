package al.hexad.assignment.ui.home.view

import al.hexad.assignment.ui.base.view.MVPView

/**
 * Created by Arbër Thaçi on 19-01-05.
 * Email: arberlthaci@gmail.com
 */

interface HomeMVPView : MVPView {

    fun setToolbarTitle(title: String)
    fun openTeamsFragment()
}