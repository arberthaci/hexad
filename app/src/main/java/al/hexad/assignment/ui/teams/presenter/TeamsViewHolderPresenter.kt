package al.hexad.assignment.ui.teams.presenter

import al.hexad.assignment.ui.teams.view.TeamsViewHolderView

/**
 * Created by Arbër Thaçi on 19-01-05.
 * Email: arberlthaci@gmail.com
 */

class TeamsViewHolderPresenter (private var teamsAdapterPresenter: TeamsAdapterPresenter) {

    fun clearViewHolder(viewHolderView: TeamsViewHolderView) = viewHolderView.let {
        it.setName()
        it.setRating()
        it.loadThumbnail()
    }

    fun onBindViewHolder(position: Int, viewHolderView: TeamsViewHolderView) = viewHolderView.let {
        val team = teamsAdapterPresenter.getItemAt(position)

        it.setName(team.name)
        it.setRating(team.rating)
        it.loadThumbnail(team.photoUrl)
        it.setOnClickListeners()
    }

    fun onClickListenerItem(position: Int) = teamsAdapterPresenter.itemClickedAtPosition(position)
}