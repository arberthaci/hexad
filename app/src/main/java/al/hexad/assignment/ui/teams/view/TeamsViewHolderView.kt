package al.hexad.assignment.ui.teams.view

/**
 * Created by Arbër Thaçi on 19-01-05.
 * Email: arberlthaci@gmail.com
 */

interface TeamsViewHolderView {

    fun setName(name: String? = null)
    fun setRating(rating: Double? = null)
    fun loadThumbnail(thumbnailUrl: String? = null)
    fun setOnClickListeners()
}