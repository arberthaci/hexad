package al.hexad.assignment.ui.teams.view

import al.hexad.assignment.data.models.Team
import al.hexad.assignment.ui.base.view.MVPView

/**
 * Created by Arbër Thaçi on 19-01-05.
 * Email: arberlthaci@gmail.com
 */

interface TeamsMVPView : MVPView {

    fun prepareLoading()
    fun displayTeamList(teams: List<Team>?): Unit?

    fun openRateTeamDialog()

    fun displayStartIconInFab()
    fun displayStopIconInFab()

    fun displayStartRandomRatingAlert()
    fun displayStopRandomRatingAlert()
    fun displayProblemRandomRatingAlert()
}