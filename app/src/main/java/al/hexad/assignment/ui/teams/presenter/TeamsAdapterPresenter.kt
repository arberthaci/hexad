package al.hexad.assignment.ui.teams.presenter

import al.hexad.assignment.data.models.Team
import al.hexad.assignment.ui.teams.view.TeamsAdapterView
import javax.inject.Inject

/**
 * Created by Arbër Thaçi on 19-01-05.
 * Email: arberlthaci@gmail.com
 */

class TeamsAdapterPresenter @Inject internal constructor(private val teamsListItems: MutableList<Team>) {

    var view: TeamsAdapterView? = null

    fun getItemCount() =
            this.teamsListItems.size

    fun getItemAt(position: Int) =
            this.teamsListItems[position]

    fun insertTeamsToList(teams: List<Team>) {
        clearList()
        loadMoreTeamsToList(teams)
    }

    fun loadMoreTeamsToList(teams: List<Team>) {
        this.teamsListItems.addAll(teams)
        notifyDataSetChanged()
    }

    fun clearList() {
        this.teamsListItems.clear()
        notifyDataSetChanged()
    }

    fun notifyDataSetChanged() =
            view?.adapterNotifyDataSetChanged()

    fun itemClickedAtPosition(position: Int) =
            view?.adapterItemClicked(position)
}