package al.hexad.assignment.ui.teams.view

import al.hexad.assignment.R
import al.hexad.assignment.ui.teams.presenter.TeamsAdapterPresenter
import al.hexad.assignment.ui.teams.presenter.TeamsViewHolderPresenter
import al.hexad.assignment.utils.extensions.loadImage
import al.hexad.assignment.utils.extensions.roundToTwoDecimals
import al.hexad.assignment.utils.rvadapterlisteners.OnTeamActionListener
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.item_teams_list.view.*

/**
 * Created by Arbër Thaçi on 19-01-05.
 * Email: arberlthaci@gmail.com
 */

class TeamsAdapter (val presenter: TeamsAdapterPresenter)
    : RecyclerView.Adapter<TeamsAdapter.TeamsViewHolder>(), TeamsAdapterView {

    init {
        presenter.view = this
    }

    lateinit var onTeamActionListener: OnTeamActionListener

    override fun getItemCount() = this.presenter.getItemCount()

    override fun onBindViewHolder(holder: TeamsViewHolder, position: Int) = holder.let {
        holder.viewHolderPresenter.clearViewHolder(it)
        holder.viewHolderPresenter.onBindViewHolder(position, it)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = TeamsViewHolder(LayoutInflater.from(parent.context)
            .inflate(R.layout.item_teams_list, parent, false))

    override fun adapterNotifyDataSetChanged() {
        notifyDataSetChanged()
    }

    override fun adapterItemClicked(position: Int) =
            onTeamActionListener.onItemSelected(position)

    inner class TeamsViewHolder(view: View) : RecyclerView.ViewHolder(view), TeamsViewHolderView {

        val viewHolderPresenter = TeamsViewHolderPresenter(presenter)

        override fun setName(name: String?) {
            name?.let { itemView.tv_team_name.text = it }
                    ?: run { itemView.tv_team_name.text = "" }
        }

        override fun setRating(rating: Double?) {
            rating?.let { itemView.tv_team_rating.text = itemView.context.getString(R.string.team_rating_placeholder, it.roundToTwoDecimals()) }
                    ?: run { itemView.tv_team_rating.text = "" }
        }

        override fun loadThumbnail(thumbnailUrl: String?) {
            thumbnailUrl?.let { itemView.iv_team_photo.loadImage(it) }
                    ?: run { itemView.iv_team_photo.setImageDrawable(null) }
        }

        override fun setOnClickListeners() {
            if (adapterPosition != RecyclerView.NO_POSITION) {
                itemView.iv_team_rate.setOnClickListener { viewHolderPresenter.onClickListenerItem(adapterPosition) }
            }
        }
    }
}