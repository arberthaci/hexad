package al.hexad.assignment.ui.base.view

import al.hexad.assignment.utils.AppConstants

/**
 * Created by Arbër Thaçi on 19-01-05.
 * Email: arberlthaci@gmail.com
 */

interface MVPView {

    fun showLoadingIndicator()

    fun dismissLoadingIndicator()

    fun showAlerter(alerterMode: AppConstants.AlerterMode, message: String)
}