package al.hexad.assignment.ui.teams.presenter

import al.hexad.assignment.data.models.Team
import al.hexad.assignment.ui.base.presenter.BasePresenter
import al.hexad.assignment.ui.teams.interactor.TeamsMVPInteractor
import al.hexad.assignment.ui.teams.view.TeamsMVPView
import al.hexad.assignment.utils.rx.SchedulerProvider
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject
import java.util.concurrent.TimeUnit
import kotlin.random.Random


/**
 * Created by Arbër Thaçi on 19-01-05.
 * Email: arberlthaci@gmail.com
 */

class TeamsPresenter <V : TeamsMVPView, I : TeamsMVPInteractor> @Inject internal constructor(interactor: I, schedulerProvider: SchedulerProvider, disposable: CompositeDisposable) : BasePresenter<V, I>(interactor = interactor, schedulerProvider = schedulerProvider, compositeDisposable = disposable), TeamsMVPPresenter<V, I> {

    private var teams: MutableList<Team> = mutableListOf()
    private var teamIndex: Int? = null
    private var randomMode = false

    override fun onViewPrepared() {
        getView()?.let {
            it.prepareLoading()
            loadTeamsIntoList()
            reOrderList()
            it.displayTeamList(teams)
        }
    }

    override fun onRateTeamClicked(position: Int) {
        teamIndex = position
        getView()?.openRateTeamDialog()
    }

    override fun onRateTeamApplied(numberOfStars: Int) {
        teamIndex?.let { rateTeam(index = it, numberOfStars = numberOfStars) }
    }

    override fun onRandomRateClicked() {
        if (randomMode) stopRandomRating()
        else startRandomRating()
    }

    private fun loadTeamsIntoList() {
        teams.add(Team(name = "Chelsea", rating = 5.0, rateCounter = 1, photoUrl = "https://upload.wikimedia.org/wikipedia/en/thumb/c/cc/Chelsea_FC.svg/200px-Chelsea_FC.svg.png"))
        teams.add(Team(name = "Manchester United", rating = 1.0, rateCounter = 1, photoUrl = "https://upload.wikimedia.org/wikipedia/en/thumb/7/7a/Manchester_United_FC_crest.svg/220px-Manchester_United_FC_crest.svg.png"))
        teams.add(Team(name = "Liverpool", rating = 4.0, rateCounter = 1, photoUrl = "https://upload.wikimedia.org/wikipedia/en/thumb/0/0c/Liverpool_FC.svg/170px-Liverpool_FC.svg.png"))
        teams.add(Team(name = "Arsenal", rating = 3.0, rateCounter = 1, photoUrl = "https://upload.wikimedia.org/wikipedia/en/thumb/5/53/Arsenal_FC.svg/180px-Arsenal_FC.svg.png"))
        teams.add(Team(name = "Manchester City", rating = 2.0, rateCounter = 1, photoUrl = "https://upload.wikimedia.org/wikipedia/en/thumb/e/eb/Manchester_City_FC_badge.svg/200px-Manchester_City_FC_badge.svg.png"))
        teams.add(Team(name = "Tottenham", rating = 1.0, rateCounter = 1, photoUrl = "https://upload.wikimedia.org/wikipedia/en/thumb/b/b4/Tottenham_Hotspur.svg/100px-Tottenham_Hotspur.svg.png"))
        teams.add(Team(name = "Everton", rating = 2.0, rateCounter = 1, photoUrl = "https://upload.wikimedia.org/wikipedia/en/thumb/7/7c/Everton_FC_logo.svg/220px-Everton_FC_logo.svg.png"))
        teams.add(Team(name = "Sunderland", rating = 4.0, rateCounter = 1, photoUrl = "https://upload.wikimedia.org/wikipedia/en/thumb/7/77/Logo_Sunderland.svg/220px-Logo_Sunderland.svg.png"))
        teams.add(Team(name = "Newcastle", rating = 3.0, rateCounter = 1, photoUrl = "https://upload.wikimedia.org/wikipedia/en/thumb/5/56/Newcastle_United_Logo.svg/200px-Newcastle_United_Logo.svg.png"))
        teams.add(Team(name = "Leicester", rating = 2.0, rateCounter = 1, photoUrl = "https://upload.wikimedia.org/wikipedia/en/thumb/2/2d/Leicester_City_crest.svg/220px-Leicester_City_crest.svg.png"))
    }

    private fun reOrderList() {
        teams.sortWith(compareByDescending { it.rating })
    }

    private fun rateTeam(index: Int, numberOfStars: Int) {
        getView()?.prepareLoading()
        val selectedTeam = teams[index]

        val currentRating = selectedTeam.rating ?: 0.0
        val currentCounter = selectedTeam.rateCounter ?: 0

        val newCounter = currentCounter + 1
        val newRating = ((currentRating * currentCounter) + numberOfStars) / newCounter
        selectedTeam.rating = newRating
        selectedTeam.rateCounter = newCounter

        teams[index] = selectedTeam
        reOrderList()
        getView()?.displayTeamList(teams)
    }

    private fun startRandomRating() {
        randomMode = true
        getView()?.let {
            it.displayStopIconInFab()
            it.displayStartRandomRatingAlert()
        }

        // Repeat random rating every X seconds, where X is a random number from 0 to 10
        compositeDisposable.add(Observable.just(randomMode)
                .repeatWhen { observable -> observable.concatMap { Observable.timer(Random.nextLong(10), TimeUnit.SECONDS) } }
                .compose(schedulerProvider.ioToMainObservableScheduler())
                .subscribe(
                        { randomlyRateATeam() },
                        { getView()?.displayProblemRandomRatingAlert() })
        )
    }

    private fun randomlyRateATeam() {
        val randomIndex = Random.nextInt(teams.size)
        val randomRate = Random.nextInt(1, 5)

        rateTeam(index = randomIndex, numberOfStars = randomRate)

    }

    private fun stopRandomRating() {
        compositeDisposable.dispose()
        randomMode = false

        getView()?.let {
            it.displayStartIconInFab()
            it.displayStopRandomRatingAlert()
        }
    }
}