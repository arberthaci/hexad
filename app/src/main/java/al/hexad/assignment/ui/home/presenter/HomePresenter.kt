package al.hexad.assignment.ui.home.presenter

import al.hexad.assignment.ui.base.presenter.BasePresenter
import al.hexad.assignment.ui.home.interactor.HomeMVPInteractor
import al.hexad.assignment.ui.home.view.HomeMVPView
import al.hexad.assignment.utils.rx.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

/**
 * Created by Arbër Thaçi on 19-01-05.
 * Email: arberlthaci@gmail.com
 */

class HomePresenter<V : HomeMVPView, I : HomeMVPInteractor> @Inject internal constructor(interactor: I, schedulerProvider: SchedulerProvider, disposable: CompositeDisposable) : BasePresenter<V, I>(interactor = interactor, schedulerProvider = schedulerProvider, compositeDisposable = disposable), HomeMVPPresenter<V, I> {

    override fun onAttach(view: V?) {
        super.onAttach(view)
        getView()?.openTeamsFragment()
    }
}