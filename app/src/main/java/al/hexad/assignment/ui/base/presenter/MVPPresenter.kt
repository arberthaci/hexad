package al.hexad.assignment.ui.base.presenter

import al.hexad.assignment.ui.base.interactor.MVPInteractor
import al.hexad.assignment.ui.base.view.MVPView

/**
 * Created by Arbër Thaçi on 19-01-05.
 * Email: arberlthaci@gmail.com
 */

interface MVPPresenter<V : MVPView, I : MVPInteractor> {

    fun onAttach(view: V?)

    fun onDetach()

    fun getView(): V?
}