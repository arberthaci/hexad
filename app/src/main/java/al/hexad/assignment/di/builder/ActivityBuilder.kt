package al.hexad.assignment.di.builder

import al.hexad.assignment.ui.home.HomeActivityModule
import al.hexad.assignment.ui.home.view.HomeActivity
import al.hexad.assignment.ui.teams.TeamsFragmentProvider
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by Arbër Thaçi on 19-01-05.
 * Email: arberlthaci@gmail.com
 */

@Module
abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = [(HomeActivityModule::class), (TeamsFragmentProvider::class)])

    abstract fun bindHomeActivity(): HomeActivity
}