package al.hexad.assignment.data.models

/**
 * Created by Arbër Thaçi on 19-01-05.
 * Email: arberlthaci@gmail.com
 */

data class Team (var name: String? = null,
                 var rating: Double? = null,
                 var rateCounter: Int? = null,
                 var photoUrl: String? = null)