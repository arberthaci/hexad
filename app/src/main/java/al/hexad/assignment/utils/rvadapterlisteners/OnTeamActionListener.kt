package al.hexad.assignment.utils.rvadapterlisteners

/**
 * Created by Arbër Thaçi on 19-01-05.
 * Email: arberlthaci@gmail.com
 */

interface OnTeamActionListener {

    fun onItemSelected(position: Int)
}