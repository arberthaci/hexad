package al.hexad.assignment.utils

/**
 * Created by Arbër Thaçi on 19-01-05.
 * Email: arberlthaci@gmail.com
 */

object AppConstants {

    enum class AlerterMode constructor(val type: Int) {
        ALERT_INFO(1),
        ALERT_WARNING(2),
        ALERT_SUCCESS(3),
        ALERT_FAILED(4)
    }

    enum class FragmentAnimationMode constructor(val type: Int) {
        ANIMATION_NONE(0),
        ANIMATION_FADE(1),
        ANIMATION_SLIDE(2)
    }
}