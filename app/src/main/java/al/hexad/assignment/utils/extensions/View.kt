package al.hexad.assignment.utils.extensions

import android.view.View
import androidx.core.view.isVisible

/**
 * Created by Arbër Thaçi on 19-01-05.
 * Email: arberlthaci@gmail.com
 */

internal fun View.show() {
    this.let { if(!it.isVisible) it.isVisible = true }
}

internal fun View.hide() {
    this.let { if(it.isVisible) it.isVisible = false }
}