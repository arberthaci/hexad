package al.hexad.assignment.utils.extensions

/**
 * Created by Arbër Thaçi on 19-01-06.
 * Email: arberlthaci@gmail.com
 */

internal fun Double.roundToTwoDecimals(): String {
    var formattedDouble = String.format("%.2f", this).trimEnd('0')
    if (formattedDouble.endsWith('.'))
        formattedDouble = formattedDouble.removeSuffix(".")
    return formattedDouble
}
